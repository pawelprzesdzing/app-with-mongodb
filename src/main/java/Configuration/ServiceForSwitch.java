package Configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceForSwitch {
    private UserChoice userChoice = new UserChoice();
    private Settings settings = new Settings();
    private boolean logout = false;

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceForSwitch.class);

    public void showMenu() {
        System.out.println("Choose one option from menu:" +
                "\n1. Show worker's collection. " +
                "\n2. Show company car's collection." +
                "\n3. Add worker." +
                "\n4. Remove worker by ID." +
                "\n5. Number of workers." +
                "\n6. Number of company cars." +
                "\n6. Show workers by own criterion." +
                "\n7. Exit.");
    }

    public void appUp() {
        while (logout == false) {
            showMenu();
            switch (userChoice.getUserChoiceFromMenu()) {
                case 1:
                    settings.loadWorkersCollection();
                    break;
                case 2:
                    settings.loadCompanyCarsCollection();
                    break;
                case 3:
                    settings.addEmployee();
                    break;
                case 4:
                    settings.deleteEmployee();
                    break;
                case 5:
                    settings.sumOfEmployees();
                    break;
                case 6:
                    settings.sortedEmployees();
                    break;
                case 7:
                    logout = true;
                    LOGGER.info("Exit from app.");
                    break;
                default:
                    LOGGER.info("There's no option like that!");
            }
        }
    }
}
