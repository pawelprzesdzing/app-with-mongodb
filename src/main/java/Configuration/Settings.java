package Configuration;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class Settings {
    // wyciagniecie bazy danych
    private MongoClient mongoClient = new MongoClient();
    private MongoDatabase mongoDatabase = mongoClient.getDatabase("firma");

    // wyciagniecie kolekcji z bazy danych
    private MongoCollection<Document> companyCarCollection = mongoDatabase.getCollection("samochody_firmowe");
    private MongoCollection<Document> workersCollection = mongoDatabase.getCollection("pracownicy");

    // wyszukiwanie
    private Document findQuery = new Document();
    private FindIterable<Document> workersDocument = workersCollection.find(findQuery);
    private FindIterable<Document> companyCarDocument = companyCarCollection.find(findQuery);
    private MongoCursor<Document> workersCursor = workersDocument.iterator();
    private MongoCursor<Document> companyCarsCursor = companyCarDocument.iterator();

    public void loadWorkersCollection(){
        Document findQuery = new Document();
        FindIterable<Document> workersDocument = workersCollection.find(findQuery);
        MongoCursor<Document> workersCursor = workersDocument.iterator();
        while (workersCursor.hasNext()){
            System.out.println(workersCursor.next().toString());
        }
    }

    public void loadCompanyCarsCollection(){
        Document findQuery = new Document();
        FindIterable<Document> companyCarsDocument = companyCarCollection.find(findQuery);
        MongoCursor<Document> companyCarsCursor = companyCarsDocument.iterator();
        while (companyCarsCursor.hasNext()){
            System.out.println(companyCarsCursor.next().toString());
        }
    }

    public void addEmployee(){

    }

    public void deleteEmployee(){

    }

    public void sumOfEmployees(){
        System.out.println("Number of workers in company: " + workersCollection.countDocuments());
    }

    public void sortedEmployees(){

    }


}
