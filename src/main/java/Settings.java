import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class Settings {
    // wyciagniecie bazy danych
    private MongoClient mongoClient = new MongoClient();
    private MongoDatabase mongoDatabase = mongoClient.getDatabase("firma");

    // wyciagniecie kolekcji z bazy danych
    private MongoCollection<Document> collectionSamochodyFirmowe = mongoDatabase.getCollection("samochody_firmowe");
    private MongoCollection<Document> collectionPracownicy = mongoDatabase.getCollection("pracownicy");

    // wyszukiwanie
    private Document findQuery = new Document();
    private FindIterable<Document> documentsPracownicy = collectionPracownicy.find(findQuery);
    private FindIterable<Document> documentsSamochodyFirmowe = collectionSamochodyFirmowe.find(findQuery);
    private MongoCursor<Document> cursorPracownicy = documentsPracownicy.iterator();
    private MongoCursor<Document> cursorSamochodyFirmowe = documentsSamochodyFirmowe.iterator();

    public void loadCollectionPracownicy(){
        Document findQuery = new Document();
        FindIterable<Document> documentsPracownicy = collectionPracownicy.find(findQuery);
        MongoCursor<Document> cursorPracownicy = documentsPracownicy.iterator();
        while (cursorPracownicy.hasNext()){
            System.out.println(cursorPracownicy.next().toString());
        }
    }

    public void loadCollectionSamochodyFirmowe(){
        Document findQuery = new Document();
        FindIterable<Document> documentsSamochodyFirmowe = collectionSamochodyFirmowe.find(findQuery);
        MongoCursor<Document> cursorSamochodyFirmowe = documentsSamochodyFirmowe.iterator();
        while (cursorSamochodyFirmowe.hasNext()){
            System.out.println(cursorSamochodyFirmowe.next().toString());
        }
    }

    public void addEmployee(){

    }

    public void deleteEmployee(){

    }

    public void sumOfEmployees(){
        System.out.println("Liczba pracownikow w firmie: " + collectionPracownicy.countDocuments());
    }

    public void sortedEmployees(){

    }


}
